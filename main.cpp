#include <algorithm>
#include <chrono>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <limits.h>

using namespace std;

template<typename T>
auto _dis(T ax, T ay, T bx, T by) {
    return abs(ax - bx) + abs(ay - by);
}

int rows, cols;
int ncars; // number of cars
int nrds; // number of rides
int bonus; // per ride bonus
size_t steps; // number of simulation steps

struct ride {
    int fromx, fromy;
    int tox, toy;
    int start;
    int finish;

    int distance;
};
ostream& operator<<(ostream& o, const ride& r) {
    return o << "([" << r.fromx << ", " << r.fromy << "], ["
             << r.tox << ", " << r.toy << "], "
             << r.start << ", " << r.finish << ")["
             << r.distance << "]";
}
vector<ride> rides;

void read_data() {
    cin >> rows >> cols >> ncars >> nrds >> bonus >> steps;
    rides.reserve(nrds);
    ride r;
    for (int i = 0; i < nrds; ++i) {
        cin >> r.fromx >> r.fromy
            >> r.tox >> r.toy
            >> r.start >> r.finish;
        r.distance = _dis(r.fromx, r.fromy, r.tox, r.toy);
        rides.push_back(r);
    }
    /*for (int i = 0; i < nrds; ++i) {
        cout << rides[i] << "\n";
    }*/
}

void print_solution(const vector<vector<int>>& sched) {
    for (int car = 0; car < sched.size(); ++car) {
        cout << sched[car].size();
        for (int rd = 0; rd < sched[car].size(); ++rd) {
            cout << " " << sched[car][rd];
        }
        cout << "\n";
    }
}

size_t score(const vector<vector<int>>& sched) {
    size_t score = 0;
    for (int car = 0; car < ncars; ++car) {
        size_t time = 0;
        int x = 0,
            y = 0;
        for (int ride = 0; ride < sched[car].size(); ++ride) {
            const auto& r = rides[sched[car][ride]];
            // drive to start point
            time += _dis(x, y, r.fromx, r.fromy);
            // test simulation end
            if (time + r.distance > steps)
                break;
            // test bonus points
            if (time <= r.start) {
                score += bonus;
                time = r.start;
            }
            // drive to ride end
            time += r.distance;
            if (time <= r.finish)
                score += r.distance;
            // update positions
            x = r.tox;
            y = r.toy;
        }
    }
    return score;
}

void solution1(vector<vector<int>>& sched) {
    sched.resize(ncars);

    vector<int> rf;
    rf.resize(nrds);
    fill(rf.begin(), rf.end(), 1);

    vector<size_t> ttf;
    ttf.resize(ncars);
    fill(ttf.begin(), ttf.end(), 0);

    auto comp = [&](auto&& a, auto&& b) {
        return ttf[a] > ttf[b];
    };
    priority_queue<int, vector<int>, decltype(comp)> car_queue{comp};
    for (int i = 0; i < ncars; ++i)
        car_queue.push(i);

    while(ttf[car_queue.top()] <= steps) {
        int car = car_queue.top();
        car_queue.pop();
        int carx = 0,
            cary = 0;
        if (sched[car].size() > 0){
            const auto& cs = sched[car];
            const auto& r = rides[cs[cs.size()-1]];
            carx = r.tox;
            cary = r.toy;
        }
        int curtime = ttf[car];

        int max_idx = -1, // current best ride index
            max_v = INT_MIN; // current best value
        bool has_rf = false; // are any rides free?
        for (int ridx = 0; ridx < nrds; ++ridx) {
            if (rf[ridx] != 1)
                continue;
            //cout << car << " " << ridx << "\n";

            const auto& r = rides[ridx];
            auto A = r.start - curtime;
            auto B = _dis(carx, cary, r.fromx, r.fromy);

            if (curtime + B + r.distance > r.finish)
                continue;
            has_rf = true;

            auto tmp = A - B;
            auto score = tmp <= 0 ? tmp : INT_MAX - tmp;
            if (score > max_v) {
                max_v = score;
                max_idx = ridx;
            }
        }
        //std::cout << "USE: " << max_idx << "\n";

        // stop processing when all rides are used
        if (!has_rf)
            break;

        const auto& r = rides[max_idx];
        sched[car].push_back(max_idx);
        ttf[car] = max(r.start - curtime,
            _dis(carx, cary, r.fromx, r.fromy)) + r.distance;
        //cout << ttf[car] << "\n";
        rf[max_idx] = 0;
        car_queue.push(car);
    }
}

void solution2(vector<vector<int>>& sched) {
    sched.resize(ncars);

    vector<int> rf;
    rf.resize(nrds);
    fill(rf.begin(), rf.end(), 1);

    vector<size_t> ttf;
    ttf.resize(ncars);
    fill(ttf.begin(), ttf.end(), 0);

    auto comp = [&](auto&& a, auto&& b) {
        return ttf[a] > ttf[b];
    };
    priority_queue<int, vector<int>, decltype(comp)> car_queue{comp};
    for (int i = 0; i < ncars; ++i)
        car_queue.push(i);

    while(ttf[car_queue.top()] <= steps) {
        int car = car_queue.top();
        car_queue.pop();
        int carx = 0,
            cary = 0;
        if (sched[car].size() > 0){
            const auto& cs = sched[car];
            const auto& r = rides[cs[cs.size()-1]];
            carx = r.tox;
            cary = r.toy;
        }
        int curtime = ttf[car];

        int max_idx1 = -1, max_idx2 = -1, // current best ride index
            max_v = INT_MIN; // current best value
        bool has_rf = false; // are any rides free?
        for (int ridx1 = 0; ridx1 < nrds; ++ridx1) {
            if (rf[ridx1] != 1)
                continue;
            for (int ridx2 = 0; ridx2 < nrds; ++ridx2) {
                if (rf[ridx2] != 1 || ridx1 == ridx2)
                    continue;
                //cout << car << " " << ridx << "\n";

                const auto& r1 = rides[ridx1];
                const auto& r2 = rides[ridx2];
                auto A1 = r1.start - curtime;
                auto B1 = _dis(carx, cary, r1.fromx, r1.fromy);
                auto A2 = r2.start - (curtime + max(A1, B1) + r1.distance);
                auto B2 = _dis(r1.tox, r1.toy, r2.fromx, r2.fromy);

                //if (curtime + B + r.distance > r.finish)
                //    continue;
                has_rf = true;

                auto tmp = A1 - B1;
                auto score1 = tmp <= 0 ? tmp : INT_MAX - tmp;
                tmp = A2 - B2;
                auto score2 = tmp <= 0 ? tmp : INT_MAX - tmp;
                auto score = score1 / 2 + score2 / 2;
                if (score > max_v) {
                    max_v = score;
                    max_idx1 = ridx1;
                    max_idx2 = ridx2;
                }
            }
        }
        //std::cout << "USE: " << max_idx << "\n";

        // stop processing when all rides are used
        if (!has_rf)
            break;

        const auto& r1 = rides[max_idx1],
            r2 = rides[max_idx2];
        sched[car].push_back(max_idx1);
        sched[car].push_back(max_idx2);
        if (curtime < r1.start)
            curtime = r1.start;
        ttf[car] = curtime + r1.distance;
        //cout << ttf[car] << "\n";
        rf[max_idx1] = 0;

        curtime = ttf[car];
        if (curtime < r2.start)
            curtime = r2.start;
        ttf[car] = curtime + r2.distance;
        //cout << ttf[car] << "\n";
        rf[max_idx2] = 0;

        car_queue.push(car);
    }
}

int main() {
    read_data();

    vector<vector<int>> sched;
    solution2(sched);
    print_solution(sched);
    //cout << "SCORE: " << score(sched) << "\n";

    return 0;
}