#!/bin/bash

for filename in input/*.in; do
    echo $filename
    ./cmake-build-release/hashcode < $filename > "$filename.out"
done
